const express = require('express');
const bodyParser = require('body-parser');
const rateLimit = require("express-rate-limit");

const { batch } = require('./batchService');

const app = express();

const limiter = rateLimit({
  windowMs: 10000,
  max: 5
});

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(limiter);

app.post('/batch', batch);

app.listen(3000, () => console.log('App is running on port 3000!'));
