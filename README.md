# batchService

Run following commands to start project:

```
$ npm i
$ npm start
```

Request example:

```
{
	"endpoint": {
		"verb": "PUT",
		"url": "https://guesty-user-service.herokuapp.com/user/{userId}"
	},
	"payload": [
		{ 
			"params": {
				"userId": 14
			},
			"data": {
				"age": 30
			} 
		},
		{ 
			"params": {
				"userId": 29
			},
			"data": {
				"age": 30
			} 
		},
		{ 
			"params": {
				"userId": 103
			},
			"data": {
				"age": 30
			} 
		}
		
	] 
}
```