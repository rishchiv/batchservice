const axios = require('axios');
const axiosRetry = require('axios-retry');

axiosRetry(axios, {retries: 1});

const batch = (req, res) => {
  const { endpoint: { verb, url }, payload } = req.body;

  const requests = payload.map(item => {
    const { data, params: { userId } } = item;

    return axios({
      method: verb,
      url: url.replace(/\{(.*?)\}/gi, userId),
      data
    });
  });

  const wrappedPromises = requests.map(promise => {
      return promise && new Promise((resolve, reject) => {
        promise.then(resolve).catch(resolve);
      });
  });

  Promise.all(wrappedPromises).then(values => {
    const result = values.reduce((acc, item) => {  
      if (item instanceof Error) {
        acc.failedRequests.push({
          status: item.response.status,
          url: item.config.url,
          error: item.response.statusText
        });
      } else {
        acc.successful++;
      }

      return acc;
    }, { successful: 0, failedRequests: [] });

    res.json(result);
  })
  .catch(err => res.status(500).json({ error: err.message ? err.message : err }));
};

module.exports = {
  batch
};
